# Cascade SFU server

This package is cascade SFU server based on [Mediasoup](https://github.com/versatica/mediasoup) project for handling WebRTC communication.

Versions 1.\*.\* are development and unstable.