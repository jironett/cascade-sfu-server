
const ProtooRoom = require('protoo-server').Room;
const SignalPeer = require('./ClientSignalPeer').SignalPeer;


class ClientSignalRoom extends ProtooRoom {

    constructor()
    {
        super();
    }

    createPeer(peerId, transport)
    {
        if (!transport)
            throw new TypeError('no transport given');

        if (!peerId || typeof peerId !== 'string')
        {
            transport.close();
            throw new TypeError('peerId must be a string');
        }

        if (this._peers.has(peerId))
        {
            transport.close();
            throw new Error(
                `there is already a peer with same peerId [peerId:"${peerId}"]`);
        }

        // Create the Peer instance.
        const peer = new SignalPeer(peerId, transport);

        // Store it in the map.
        this._peers.set(peer.id, peer);

        // Handle peer.
        this._handlePeer(peer);

        return peer;
    }
}

module.exports = ClientSignalRoom;

