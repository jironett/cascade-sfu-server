
const ProtooPeer = require('protoo-server/lib/Peer');

class ClientSignalPeer extends ProtooPeer {

    constructor(peerId, transport)
    {
        super(peerId, transport);
    }

}

module.exports = ClientSignalPeer;